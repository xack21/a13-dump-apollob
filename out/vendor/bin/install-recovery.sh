#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/by-name/recovery:134217728:2a5d30a50c3878504b48a2815a5ef3d3695bdee4; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/by-name/boot:33554432:39c86e7144cce13a0d63130a83b9f57ae79794f0 \
          --target EMMC:/dev/block/by-name/recovery:134217728:2a5d30a50c3878504b48a2815a5ef3d3695bdee4 && \
      log -t recovery "Installing new oplus recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oplus recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi
